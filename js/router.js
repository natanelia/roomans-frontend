app.config(function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/");

  $stateProvider
    .state('schedule', {
        url: '/',
        templateUrl: '/template/schedule.html',
        controller: 'ScheduleCtrl'
    })
    .state('rooms', {
        url: '/rooms',
        templateUrl: '/template/rooms.html',
        controller: 'RoomsCtrl'
    })
    .state('lessons', {
        url: '/lessons',
        templateUrl: '/template/lessons.html',
        controller: 'LessonsCtrl'
    })
    .state('statistic', {
        url: '/statistic',
        templateUrl: '/template/statistic.html',
        controller: 'StatisticCtrl'
    })
});
