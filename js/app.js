var app = angular.module('roomans', ['ui.bootstrap', 'ui.router', 'chart.js']);

function parseDate(mySQLDate) {
    return new Date(Date.parse(mySQLDate.replace('-','/','g')));
}

function parseTime(hour) {
	hour = hour + '';
	if (hour.length < 2)
		hour = '0' + hour;

	return hour + ':00';
}


function augmentZero(str) {
    str = str + '';
    if (str.length < 2) {
        return '0' + str;
    }
    return str;
};

function convertToSQLDate(date) {
    return date.getFullYear() + '-' + augmentZero(date.getMonth() + 1) + '-' + augmentZero(date.getDate());
}