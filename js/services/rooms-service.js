app.factory('RoomService', function($http, $q) {
    var factory = {};
    var endpoint = 'http://localhost:3000/ruangan/';

    factory.getAllRooms = function() {
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: endpoint
        }).success(function(data) {
            deferred.resolve(data);
        });

        return deferred.promise;
    };

    factory.addRoom = function(dat) {
        var deferred = $q.defer();

        var req = {
            kode_ruangan: dat.name,
            kapasitas: dat.capacity
        };

        //kode_ruangan, kapasitas, status_kondisi
        $http({
            method: 'POST',
            url: endpoint,
            data: req,
            headers: {'content-type': 'application/json'}
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    };

    factory.editRoom = function(dat) {
        var deferred = $q.defer();

        var req = {
            id: dat.id,
            kode_ruangan: dat.name,
            kapasitas: dat.capacity,
            status_kondisi: dat.condition
        };

        $http({
            method: 'PUT',
            url: endpoint + req.id,
            data: req,
            headers: {'content-type': 'application/json'}
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    factory.deleteRoom = function(id) {
        var deferred = $q.defer();

        $http({
            method: 'DELETE',
            url: endpoint + id,
            headers: {'content-type': 'application/json'}
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    return factory;
});