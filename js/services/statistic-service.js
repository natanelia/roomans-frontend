app.factory('StatisticService', function($http, $q) {
    var factory = {};
    var endpoint = 'http://localhost:3000/';

    factory.getRoomsUsage = function(year) {
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: endpoint + 'statistik/booking/' + year
        })
        .then(function(data) {
            deferred.resolve(data.data);
        }, function(data) {
            deferred.reject('Gagal mendapat statistik penggunaan ruangan.' + (data.data.message ? data.data.message : ''));
        });

        return deferred.promise;
    };

    factory.getRoomsUsageByGroup = function(year) {
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: endpoint + 'statistik/keluser/' + year
        })
        .then(function(data) {
            deferred.resolve(data.data);
        }, function(data) {
            deferred.reject('Gagal mendapat statistik penggunaan ruangan berdasarkan kelompok jurusan.' + (data.data.message ? data.data.message : ''));
        });

        return deferred.promise;
    };

    factory.getBrokenRooms = function(year) {
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: endpoint + 'statistik/perbaikan/' + year
        })
        .then(function(data) {
            deferred.resolve(data.data);
        }, function(data) {
            deferred.reject('Gagal mendapat statistik perbaikan ruangan.' + (data.data.message ? data.data.message : ''));
        });

        return deferred.promise;
    };

    return factory;
});