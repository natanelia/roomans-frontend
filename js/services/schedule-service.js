app.factory('ScheduleService', function($http, $q) {
    var factory = {};
    var endpoint = 'http://localhost:3000/booking/';
    var mEndPoint = 'http://localhost:3000/perbaikan/';

    factory.getSchedule = function(roomId, startDate, endDate) {
        var deferred = $q.defer();

        endDate.setDate(endDate.getDate() + 1);

        $http({
            method: 'POST',
            url: endpoint + roomId,
            data: {
                start_date: convertToSQLDate(startDate),
                end_date: convertToSQLDate(endDate)
            }
        }).success(function(data) {
            deferred.resolve(data.data);
        });

        return deferred.promise;
    };

    factory.getValidRooms = function(dat) {
        var deferred = $q.defer();

        var req = {
            occurence: dat.occurence,
            kuliah_id: dat.lessonId,
            kelas_no: dat.classNo,
            start_date: moment(dat.startDate).format('YYYY-MM-DD'),
            end_date: moment(dat.endDate).format('YYYY-MM-DD'),
            start_hour: moment(dat.startHour).format('HH:mm:ss'),
            end_hour: moment(dat.endHour).format('HH:mm:ss')
        };

        $http({
            method: 'POST',
            url: endpoint + 'validRooms/get',
            data: req
        }).success(function(data) {
            deferred.resolve(data.data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    };

    factory.addSchedule = function(dat) {
        var deferred = $q.defer();

        var req = {
            ruangan_id: dat.roomId,
            occurence: dat.occurence,
            kuliah_id: dat.lessonId,
            kelas_no: dat.classNo,
            start_date: moment(dat.startDate).format('YYYY-MM-DD'),
            end_date: moment(dat.endDate).format('YYYY-MM-DD'),
            start_hour: moment(dat.startHour).format('HH:mm:ss'),
            end_hour: moment(dat.endHour).format('HH:mm:ss')
        };

        $http({
            method: 'POST',
            url: endpoint,
            data: req
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    };


    factory.addMaintenance = function(dat) {
        var deferred = $q.defer();

        var req = {
            ruangan_id: dat.roomId,
            start_date: moment(dat.startDate).format('YYYY-MM-DD'),
            end_date: moment(dat.startDate).format('YYYY-MM-DD'),
            start_hour: moment(dat.startHour).format('HH:mm:ss'),
            end_hour: moment(dat.endHour).format('HH:mm:ss')
        };

        $http({
            method: 'POST',
            url: mEndPoint,
            data: req
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    };

    factory.editSchedule = function(roomId, dat) {
        var deferred = $q.defer();

        var req = {
            ruangan_id: roomId,
            occurence: 'ONCE',
            kuliah_id: dat.lessonId,
            kelas_no: dat.classNo,
            start_date: moment(dat.startDate).format('YYYY-MM-DD'),
            end_date: moment(dat.startDate).format('YYYY-MM-DD'),
            start_hour: moment(dat.startHour).format('HH:mm:ss'),
            end_hour: moment(dat.endHour).format('HH:mm:ss')
        };

        $http({
            method: 'PUT',
            url: endpoint + dat.scheduleId,
            data: req
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    };

    factory.editMaintenance = function(roomId, dat) {
        var deferred = $q.defer();

        var req = {
            ruangan_id: roomId,
            start_date: moment(dat.startDate).format('YYYY-MM-DD'),
            end_date: moment(dat.endDate).format('YYYY-MM-DD'),
            start_hour: moment(dat.startHour).format('HH:mm:ss'),
            end_hour: moment(dat.endHour).format('HH:mm:ss')
        };

        $http({
            method: 'PUT',
            url: mEndPoint + dat.perbaikan_no,
            data: req
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    };

    factory.deleteSchedule = function(scheduleId) {
        var deferred = $q.defer();

        $http({
            method: 'DELETE',
            url: endpoint + scheduleId,
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    factory.deleteMaintenance = function(maintenanceId) {
        var deferred = $q.defer();

        $http({
            method: 'DELETE',
            url: mEndPoint + maintenanceId,
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    return factory;
});