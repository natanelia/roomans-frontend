app.factory('ClassService', function($http, $q) {
    var factory = {};
    var lessonEndpoint = 'http://localhost:3000/kuliah/';

    var getEndpoint = function(lessonId) {
        return lessonEndpoint + lessonId + '/kelas/';
    }

    factory.getAllClasses = function(lessonId) {
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: getEndpoint(lessonId)
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    };

    factory.addClass = function(lessonId, dat) {
        var deferred = $q.defer();

        var req = {
            jumlah_mahasiswa: dat.studentCount,
            dosen: dat.lecturer
        };

        //kode_ruangan, kapasitas, status_kondisi
        $http({
            method: 'POST',
            url: getEndpoint(lessonId),
            data: req,
            headers: {'content-type': 'application/json'}
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    };

    factory.editClass = function(lessonId, dat) {
        var deferred = $q.defer();

        var req = {
            kelas_no: dat.id,
            jumlah_mahasiswa: dat.studentCount,
            dosen: dat.lecturer
        };

        $http({
            method: 'PUT',
            url: getEndpoint(lessonId) + req.kelas_no,
            data: req,
            headers: {'content-type': 'application/json'}
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    factory.deleteClass = function(lessonId, classId) {
        var deferred = $q.defer();

        $http({
            method: 'DELETE',
            url: getEndpoint(lessonId) + classId,
            headers: {'content-type': 'application/json'}
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    return factory;
});