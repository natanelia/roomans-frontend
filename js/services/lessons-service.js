app.factory('LessonService', function($http, $q) {
    var factory = {};
    var endpoint = 'http://localhost:3000/kuliah/';

    factory.getAllLessons = function() {
        var deferred = $q.defer();

        $http({
            method: 'GET',
            url: endpoint
        }).success(function(data) {
            deferred.resolve(data);
        });

        return deferred.promise;
    };

    factory.addLesson = function(dat) {
        var deferred = $q.defer();

        var req = {
            kode_kuliah: dat.code,
            nama_kuliah: dat.name,
            kelompok_jurusan: dat.course
        };

        //kode_ruangan, kapasitas, status_kondisi
        $http({
            method: 'POST',
            url: endpoint,
            data: req,
            headers: {'content-type': 'application/json'}
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    };

    factory.editLesson = function(dat) {
        var deferred = $q.defer();

        var req = {
            id: dat.id,
            kode_kuliah: dat.code,
            nama_kuliah: dat.name,
            kelompok_jurusan: dat.course
        };

        $http({
            method: 'PUT',
            url: endpoint + req.id,
            data: req,
            headers: {'content-type': 'application/json'}
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    factory.deleteLesson = function(id) {
        var deferred = $q.defer();

        $http({
            method: 'DELETE',
            url: endpoint + id,
            headers: {'content-type': 'application/json'}
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });

        return deferred.promise;
    }

    return factory;
});