app.controller('ScheduleCtrl', function($scope, $rootScope, ScheduleService, LessonService, RoomService) {
    $rootScope.$broadcast("active", "schedule");

    var hideModal = function(domId) {
        $(domId).modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }

    var getSchedule = function(roomId, startDate, endDate) {
        ScheduleService.getSchedule(roomId, startDate, endDate).then(function(data) {
            $scope.editData = angular.copy(data);
            for (var i in $scope.editData.days) {
                for (var j in $scope.editData.days[i].scheduled) {
                    $scope.editData.days[i].scheduled[j].startDate = new Date($scope.editData.days[i].scheduled[j].date);
                    var a = $scope.editData.days[i].scheduled[j].startHour;
                    $scope.editData.days[i].scheduled[j].startHour = new Date($scope.editData.days[i].scheduled[j].date);
                    $scope.editData.days[i].scheduled[j].startHour.setHours(a);

                    var b = $scope.editData.days[i].scheduled[j].endHour;
                    $scope.editData.days[i].scheduled[j].endHour = new Date($scope.editData.days[i].scheduled[j].date);
                    $scope.editData.days[i].scheduled[j].endHour.setHours(b);

                    $scope.editData.days[i].scheduled[j].availableRooms = data.rooms;

                    delete($scope.editData.days[i].scheduled[j].date);
                }


                for (var j in $scope.editData.days[i].maintenances) {
                    $scope.editData.days[i].maintenances[j].startDate = new Date($scope.editData.days[i].maintenances[j].date);
                    var a = $scope.editData.days[i].maintenances[j].startHour;
                    $scope.editData.days[i].maintenances[j].startHour = new Date($scope.editData.days[i].maintenances[j].date);
                    $scope.editData.days[i].maintenances[j].startHour.setHours(a);

                    var b = $scope.editData.days[i].maintenances[j].endHour;
                    $scope.editData.days[i].maintenances[j].endHour = new Date($scope.editData.days[i].maintenances[j].date);
                    $scope.editData.days[i].maintenances[j].endHour.setHours(b);

                    $scope.editData.days[i].maintenances[j].availableRooms = data.rooms;

                    delete($scope.editData.days[i].maintenances[j].date);
                }
            }

            for (var i in data.days) {
                for (var j in data.days[i].scheduled) {
                    data.days[i].scheduled[j].date = moment(new Date(data.days[i].scheduled[j].date)).format('YYYY-MM-DD');
                }
                for (var j in data.days[i].maintenances) {
                    data.days[i].maintenances[j].date = moment(new Date(data.days[i].maintenances[j].date)).format('YYYY-MM-DD');
                }
            }

            $scope.data = data;
            $scope.data.startDate = startDate;
            $scope.data.endDate = endDate;
            $scope.data.endDate.setDate($scope.data.endDate.getDate() - 1);

            $scope.availableHours = [];
            for (var i = $scope.data.startHour; i < $scope.data.endHour; i++) {
                $scope.availableHours.push(i);
            }

            $scope.data.dateRange = getDateInfoString(startDate, endDate);
            $scope.currentRoomId = data.roomId;


            var nowDate = new Date();
            nowDate.setSeconds(0);
            nowDate.setMilliseconds(0);
            nowDate.setMinutes(0);
            var sHour = new Date(nowDate);
            if (sHour.getHours() > $scope.data.endHour - 1) {
                sHour.setHours($scope.data.endHour - 1);
            } else if (sHour.getHours() < $scope.data.startHour) {
                sHour.setHours($scope.data.startHour);
            }
            var eHour = new Date(sHour);
            eHour.setHours(eHour.getHours() + 1);
            $scope.newSchedule = {
                lessonId: 1,
                classNo: 1,
                occurence: "ONCE",
                startDate: new Date(nowDate),
                endDate: new Date(nowDate),
                startHour: new Date(sHour),
                endHour: new Date(eHour),
                roomId: $scope.data.roomId,
                availableRooms: []
            };

            $scope.newMaintenance = {
                startDate: new Date(nowDate),
                endDate: new Date(nowDate),
                startHour: new Date(sHour),
                endHour: new Date(eHour),
                roomId: $scope.data.roomId,
            };

            ScheduleService.getValidRooms($scope.newSchedule).then(function(res) {
                $scope.newSchedule.availableRooms = res;
            });
        });
    }

    $scope.$watch('newSchedule', function() {
        if (typeof $scope.newSchedule !== 'undefined') {
            var classes = $scope.getCurrentClasses($scope.newSchedule.lessonId);
            if ($scope.newSchedule.classNo === null && classes.length > 0) {
                $scope.newSchedule.classNo = classes[0].id;
            }

            ScheduleService.getValidRooms($scope.newSchedule).then(function(res) {
                if (res.length === 0) {
                    ScheduleService.getValidRooms($scope.newSchedule).then(function(res) {
                        $scope.newSchedule.availableRooms = res;
                    });
                } else {
                    $scope.newSchedule.availableRooms = res;
                }
            });

            if ($scope.newSchedule.occurence === "ONCE") {
                $scope.newSchedule.endDate = new Date($scope.newSchedule.startDate);
            }
        }
    }, true);


    var getDateInfoString = function(startDate, endDate) {
        var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        var dateInfoString = '';
        dateInfoString += startDate.getDate() + ' ';
        if (startDate.getMonth() !== endDate.getMonth()) {
            dateInfoString += months[startDate.getMonth()] + ' ';

            if (startDate.getFullYear() !== endDate.getFullYear()) {
                dateInfoString += startDate.getFullYear() + ' ';
            }
        }
        dateInfoString += '- ' + endDate.getDate() + ' ' + months[endDate.getMonth()] + ' ' + endDate.getFullYear();

        return dateInfoString;
    };

    LessonService.getAllLessons().then(function(res) {
        var data = res.data;
        $scope.lessons = data;
    });

    $scope.error = {
        onAddingSchedule: '',
        onModifyingSchedule: '',
        onAddingMaintenance: '',
        onModifyingMaintenance: ''
    };

    var d = new Date();
    var dayOfWeek = d.getDay();
    $scope.currentStartDate = new Date();
    $scope.currentEndDate = new Date();

    $scope.currentRoomId = 1;
    $scope.currentStartDate.setDate(d.getDate() - dayOfWeek);
    $scope.currentEndDate.setDate(d.getDate() + 7 - dayOfWeek - 1);

    RoomService.getAllRooms().then(function(res) {
        $scope.currentRoomId = res.data.rooms[0].id;
        getSchedule($scope.currentRoomId, $scope.currentStartDate, $scope.currentEndDate);
    });

    $scope.currentDate = d.getFullYear() + '-' + augmentZero(d.getMonth()) + '-' + augmentZero(d.getDay());

    $scope.isToday = function(dayId) {
        var sDate = new Date($scope.data.startDate);
        sDate.setDate(sDate.getDate() + dayId);
        return (sDate.getDate() === d.getDate() && sDate.getMonth() === d.getMonth() && sDate.getFullYear() === d.getFullYear());
    };

    $scope.parseTime = function(hour) {
        return parseTime(hour);
    };

    $scope.getCurrentClasses = function(lessonId) {
        for (var i in $scope.lessons) {
            if ($scope.lessons[i].id === lessonId) {
                return $scope.lessons[i].classes;
            }
        }
    };

    $scope.getRoomCondition = function(roomId) {
        if ($scope.data) {
            return _.find($scope.data.rooms, {id: roomId}).condition;
        } else {
            return 'Baik';
        }
    }

    $scope.showAddSchedule = function(dayOfWeek, hour) {
        var startDate = new Date($scope.data.startDate);
        startDate.setDate(startDate.getDate() + dayOfWeek);
        var endDate = new Date(startDate);
        var startHour = new Date(startDate);
        startHour.setMinutes(0); startHour.setSeconds(0); startHour.setMilliseconds(0);
        startHour.setHours(hour);
        var endHour = new Date(startHour);
        endHour.setHours(hour+1);
        var roomId = $scope.currentRoomId;

        $scope.newSchedule.startDate = startDate;
        $scope.newSchedule.endDate = endDate;
        $scope.newSchedule.startHour = startHour;
        $scope.newSchedule.endHour = endHour;
        $scope.newSchedule.roomId = roomId;

        $('#modal-add-schedule').modal('show');
    }

    //////////////////////////////////////////////////
    $scope.showPreviousWeek = function() {
        var startDate = $scope.currentStartDate;
        var endDate = $scope.currentEndDate;
        startDate.setDate(startDate.getDate() - 7);
        endDate.setDate(endDate.getDate() - 7);
        getSchedule($scope.currentRoomId, startDate, endDate);
    };

    $scope.showNextWeek = function() {
        var startDate = $scope.currentStartDate;
        var endDate = $scope.currentEndDate;
        startDate.setDate(startDate.getDate() + 7);
        endDate.setDate(endDate.getDate() + 7);
        getSchedule($scope.currentRoomId, startDate, endDate);

    };

    $scope.changeRoom = function(roomId) {
        getSchedule(roomId, $scope.currentStartDate, $scope.currentEndDate);
    };

    $scope.getValidRooms = function(schedule) {
        if (schedule.jenis === 'booking') {
            schedule.occurence = 'ONCE';
            schedule.endDate = new Date(schedule.startDate);
            schedule.availableRooms = [];
            ScheduleService.getValidRooms(schedule).then(
                function(res) {
                    schedule.availableRooms = res;
                    schedule.availableRooms.push(_.find($scope.data.rooms, {id:schedule.roomId}));
                    schedule.availableRooms.sort(function(a,b) {return a.id - b.id;});
                }
            );
        }
    };

    $scope.addSchedule = function(schedule) {
        ScheduleService.addSchedule(schedule).then(
            function(res) {
                $scope.error.onAddingSchedule = '';
                hideModal('#modal-add-schedule');

                getSchedule($scope.currentRoomId, $scope.data.startDate, $scope.data.endDate);
            },
            function(res) {
                $scope.error.onAddingSchedule = res.message;
            }
        );
    };

    $scope.addMaintenance = function(maintenance) {
        ScheduleService.addMaintenance(maintenance).then(
            function(res) {
                $scope.error.onAddingMaintenance = '';
                hideModal('#modal-add-maintenance');

                getSchedule($scope.currentRoomId, $scope.data.startDate, $scope.data.endDate);
            },
            function(res) {
                $scope.error.onAddingMaintenance = res.message;
            }
        );
    };

    $scope.saveSchedule = function(roomId, schedule) {
        schedule.endDate = new Date(schedule.startDate);
        ScheduleService.editSchedule(roomId, schedule).then(
            function(res) {
                $scope.error.onModifyingSchedule = '';
                hideModal('[id^="modal-edit-schedule-"]');

                console.log($scope.data);
                getSchedule($scope.currentRoomId, $scope.data.startDate, $scope.data.endDate);
            },
            function(res) {
                $scope.error.onModifyingSchedule = res.message;
            }
        );
    };

    $scope.saveMaintenance = function(roomId, maintenance) {
        maintenance.endDate = new Date(maintenance.startDate);
        ScheduleService.editMaintenance(roomId, maintenance).then(
            function(res) {
                $scope.error.onModifyingMaintenance = '';
                hideModal('[id^="edit-maintenance-"]');

                getSchedule($scope.currentRoomId, $scope.data.startDate, $scope.data.endDate);
            },
            function(res) {
                $scope.error.onModifyingMaintenance = res.message;
            }
        );
    };

    $scope.deleteSchedule = function(scheduleId) {
        if (confirm("Apakah Anda benar-benar ingin menghapus jadwal ini?")) {
            ScheduleService.deleteSchedule(scheduleId).then(
                function(res) {
                    $scope.error.onModifyingSchedule = '';
                    hideModal('[id^="modal-edit-schedule-"]');

                    getSchedule($scope.currentRoomId, $scope.data.startDate, $scope.data.endDate);
                },
                function(res) {
                    $scope.error.onModifyingSchedule = res.message;
                }
            );
        }
    };


    $scope.deleteMaintenance = function(maintenanceId) {
        if (confirm("Apakah Anda benar-benar ingin menghapus perbaikan ini?")) {
            ScheduleService.deleteMaintenance(maintenanceId).then(
                function(res) {
                    $scope.error.onModifyingMaintenance = '';
                    hideModal('[id^="edit-maintenance-"]');

                    getSchedule($scope.currentRoomId, $scope.data.startDate, $scope.data.endDate);
                },
                function(res) {
                    $scope.error.onModifyingMaintenance = res.message;
                }
            );
        }
    };
});

