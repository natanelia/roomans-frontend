app.controller("MainCtrl", function ($scope, $rootScope) {
  $scope.active = "dashboard";

  $scope.init = function () {
    $rootScope.$on("active", function (event, val) {
      console.log(val);
      $scope.active = val;
    });
  };

  $scope.init();
});
