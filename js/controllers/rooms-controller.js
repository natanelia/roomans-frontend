app.controller('RoomsCtrl', function($scope, $rootScope, RoomService) {
    $rootScope.$broadcast("active", "rooms");

    $scope.searchQuery = '';
    $('[data-toggle="tooltip"]').tooltip();

    $scope.newRoom = {
    	name: '',
    	capacity: 30
    }

    RoomService.getAllRooms().then(function(res) {
        var data = res.data;
        $scope.frontData = angular.copy(data);
        $scope.data = angular.copy(data);
    });

    $scope.error = {
        onAddingRoom: '',
        onModifyingRoom: ''
    };

    $scope.addRoom = function(room) {
    	RoomService.addRoom(room).then(function(r) {
            $scope.error.onAddingRoom = '';
            $('#modal-add-room').modal('hide');
            RoomService.getAllRooms().then(function(res) {
                var data = res.data;
                $scope.frontData = angular.copy(data);
                $scope.data = angular.copy(data);
            });

            $scope.newRoom = {
                name: '',
                capacity: 30
            }
        }, function(r) {
            $scope.error.onAddingRoom = r.message;
        });
    }

    $scope.saveRoom = function(room) {
        RoomService.editRoom(room).then(function(r) {
            $scope.error.onModifyingRoom = '';
            $('#modal-modify-room-' + room.id).modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            RoomService.getAllRooms().then(function(res) {
                var data = res.data;
                $scope.frontData = angular.copy(data);
                $scope.data = angular.copy(data);
            });
        }, function(r) {
            $scope.error.onModifyingRoom = r.message;
        });
    }

    $scope.deleteRoom = function(roomId) {
        if (confirm("Apakah Anda benar-benar ingin menghapus ruangan ini?")) {
            RoomService.deleteRoom(roomId).then(function(r) {
                $scope.error.onModifyingRoom = '';
                $('#modal-modify-room-' + roomId).modal('hide');
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');
                RoomService.getAllRooms().then(function(res) {
                    var data = res.data;
                    $scope.frontData = angular.copy(data);
                    $scope.data = angular.copy(data);
                });
            }, function(r) {
                $scope.error.onModifyingRoom = r.message;
            });
        }
    }

});
