app.controller('StatisticCtrl', function($scope, $rootScope, StatisticService) {
    $rootScope.$broadcast("active", "statistic");

    $scope.d = new Date();
    $scope.currentYear = $scope.d.getFullYear();
    var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    $scope.data = {
        years: []
    };

    for (var i = $scope.d.getFullYear(); i >= $scope.d.getFullYear() - 5; i--) {
        $scope.data.years.push(i);
    }

    $scope.error = {
        onGettingRoomsUsage: '',
        onGettingRoomsUsageByGroup: '',
        onGettingBrokenRooms: ''
    };

    $scope.changeYear = function(year) {
        StatisticService.getRoomsUsage(year).then(function(data) {
            $scope.roomUsageStatistic = {
                data: [data.data],
                labels: months
            };
        }, function(message) {
            $scope.error.onGettingRoomsUsage = message;
        });

        StatisticService.getRoomsUsageByGroup(year).then(function(data) {
            if (data.label.length === 0) {
                data.label = ['TIDAK ADA DATA']
            }
            $scope.roomUsageByGroupStatistic = {
                data: [data.data],
                labels: data.label
            };
        }, function(message) {
            $scope.error.onGettingRoomsUsageByGroup = message;
        });

        StatisticService.getBrokenRooms(year).then(function(data) {
            $scope.brokenRoomsStatistic = {
                data: [data.data],
                labels: months
            };
        }, function(message) {
            $scope.error.onGettingBrokenRooms = message;
        });
    };
    $scope.changeYear($scope.currentYear);

});
