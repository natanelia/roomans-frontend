app.controller('LessonsCtrl', function($scope, $rootScope, LessonService, ClassService) {
    $rootScope.$broadcast("active", "lessons");
    $scope.searchQuery = '';

    $scope.isCollapsed = true;
    $scope.newLesson = {
        code: '',
        name: '',
        course: ''
    };

    var hideModal = function(domId) {
        $(domId).modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }

    var getAllLessons = function() {
        LessonService.getAllLessons().then(function(res) {
            var data = res.data;
            $scope.frontData = angular.copy(data);

            for (var i in data) {
                data[i].newClass = {
                    lessonId: data[i].id,
                    studentCount: 0,
                    lecturer: ''
                };
            }

            $scope.data = angular.copy(data);
        });
    };
    getAllLessons();

    $scope.error = {
        onAddingLesson: '',
        onModifyingLesson: '',
        onAddingClass: '',
        onModifyingClass: '',
    };

    $scope.addLesson = function(lesson) {
        LessonService.addLesson(lesson).then(
            function(res) {
                $scope.error.onAddingLesson = '';
                $('#modal-add-lesson').modal('hide');

                getAllLessons();
            },
            function(res) {
                $scope.error.onAddingLesson = res.message;
            }
        );
    };

    $scope.saveLesson = function(lesson) {
        LessonService.editLesson(lesson).then(
            function(res) {
                $scope.error.onModifyingLesson = '';
                hideModal('#modal-modify-lesson-' + lesson.id);

                getAllLessons();
            },
            function(res) {
                $scope.error.onModifyingLesson = res.message;
            }
        );
    };

    $scope.deleteLesson = function(lessonId) {
        if (confirm("Apakah Anda benar-benar ingin menghapus mata kuliah ini?")) {
            LessonService.deleteLesson(lessonId).then(
                function(res) {
                    $scope.error.onModifyingLesson = '';
                    hideModal('#modal-modify-lesson-' + lessonId);

                    getAllLessons();
                },
                function(res) {
                    $scope.error.onModifyingLesson = res.message;
                }
            );
        }
    };

    $scope.addClass = function(cls) {
        ClassService.addClass(cls.lessonId, cls).then(
            function(res) {
                $scope.error.onAddingClass = '';
                hideModal('#modal-add-class-' + cls.lessonId);

                getAllLessons();
            },
            function(res) {
                $scope.error.onAddingClass = res.message;
            }
        );
    };

    $scope.saveClass = function(cls) {
        ClassService.editClass(cls.kuliah_id, cls).then(
            function(res) {
                $scope.error.onModifyingClass = '';
                hideModal('#modal-modify-class-' + cls.kuliah_id + '-' + cls.id);

                getAllLessons();
            },
            function(res) {
                $scope.error.onModifyingClass = res.message;
            }
        );
    };

    $scope.deleteClass = function(lessonId, classId) {
        if (confirm("Apakah Anda benar-benar ingin menghapus kelas K" + classId + "?")) {
            ClassService.deleteClass(lessonId, classId).then(
                function(res) {
                    $scope.error.onModifyingClass = '';
                    hideModal('#modal-modify-class-' + lessonId + '-' + classId);

                    getAllLessons();
                },
                function(res) {
                    $scope.error.onModifyingClass = res.message;
                }
            );
        }
    };


});
